<?php

namespace App\Http\Controllers\App\Api\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return $this->response->array($users->toArray());   
    }
      
    public function show($id)
    {
        $user = User::findOrFail($id);
        return $this->response->array($user->toArray());   
    }
}
