<?php

namespace App\Http\Controllers\App\Api\V2\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\MyNew;
use App\User;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
 

class NewController extends HomeController
{
    //

    public function index()
	{
        $news = MyNew::all();
        return $this->response->array($news->toArray());   

        
    }
    
    public function show($id)
	{
        $new = MyNew::findOrFail($id);
        return $this->response->array($new->toArray());

    }

    public function update(Request $request,$id)
    {
        $new = MyNew::find($id);
        $rules = [
            'name' => ['required', 'alpha'],
            'writer' => ['required', 'exists:users,id'],    //..id in user table
            'description' => ['required', 'min:10']
            
        ];
        $payload = app('request')->only('name', 'description', 'writer');
        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            throw new UpdateResourceFailedException('Could not update new.', $validator->errors());
        }

        if ($new->updated_at > app('request')->get('last_updated')) {
            throw new ConflictHttpException('New was updated prior to your request.');
        }

        $name = app('request')->only('name');
        $writer = app('request')->only('writer');
        $description = app('request')->only('description');
        $action = "write";

        $new->name = $name;
        $new->description = $description;
        $user = User::find($writer);
        $new->users()->attach($user, ['action' => $action]);
        $new->save();

        return $this->response->array($new->toArray());
    }

    public function store(Request $request)
	{
        $rules = [
            'name' => ['required', 'alpha'],
            'writer' => ['required', 'exists:users,id'],
            'description' => ['required', 'min:10']
            
        ];

        $payload = app('request')->only('name', 'description', 'writer');
        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new.', $validator->errors());
        }

        // Create user as per usual.  
        $name = app('request')->only('name');
        $writer = app('request')->only('writer');
        $description = app('request')->only('description');
        $action = "write";
        
        $new = new MyNew;
        $new->name = $name;
        $new->description = $description;
        $user = User::find($writer);
        $new->users()->attach($user, ['action' => $action]);
        $new->save();


        return $this->response->array($new->toArray());  
    }


    public function destroy($id)
    {
        $new = MyNew::findOrFail($id);
        $new->delete();
        return response()->json("Tuple have been deleted successfully", 202);
    }
}
