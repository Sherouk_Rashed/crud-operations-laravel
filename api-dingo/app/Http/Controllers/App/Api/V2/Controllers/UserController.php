<?php

namespace App\Http\Controllers\App\Api\V2\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\User;
use App\MyNew;


class UserController extends HomeController
{
    //

    public function index()
	{
        $users = User::all();
        return $this->response->array($users->toArray());   
        
    }
    
    public function show($id)
	{
        $user = User::findOrFail($id);
        return $this->response->array($user->toArray());

    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ($user->updated_at > app('request')->get('last_updated')) {
            throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('User was updated prior to your request.');
        }

        $user->update($request->all());
        $user->save();
        return $this->response->array($user->toArray());
    }
    public function store(Request $request)
	{
        $user = new User();
        $user->create($request->all());
        $user->save();
        return $this->response->array($user->toArray());  
    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json("Tuple have been deleted successfully", 202);
    }
}
