<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


















// --------------------------------------------DINGO END POINTS-------------------------------------------------
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->get('users', 'App\Http\Controllers\App\Api\Controllers\UserController@index')->name('users.index');
    $api->get('users/{id}', 'App\Http\Controllers\App\Api\Controllers\UserController@show')->name('users.show');

    

});

$api->version('v2', function ($api) {
    $api->get('users', 'App\Http\Controllers\App\Api\V2\Controllers\UserController@index')->name('users.index');
    $api->get('users/{id}', 'App\Http\Controllers\App\Api\V2\Controllers\UserController@show')->name('users.show');
    $api->post('users', 'App\Http\Controllers\App\Api\V2\Controllers\UserController@store')->name('users.store');
    $api->put('users/{id}', 'App\Http\Controllers\App\Api\V2\Controllers\UserController@update')->name('users.update');
    $api->delete('users/{id}', 'App\Http\Controllers\App\Api\V2\Controllers\UserController@destroy')->name('users.destroy');

    $api->get('news', 'App\Http\Controllers\App\Api\V2\Controllers\NewController@index')->name('news.index');
    $api->get('news/{id}', 'App\Http\Controllers\App\Api\V2\Controllers\NewController@show')->name('news.show');
    $api->post('news', 'App\Http\Controllers\App\Api\V2\Controllers\NewController@store')->name('news.store');
    $api->put('news/{id}', 'App\Http\Controllers\App\Api\V2\Controllers\NewController@update')->name('news.update');
    $api->delete('news/{id}', 'App\Http\Controllers\App\Api\V2\Controllers\NewController@destroy')->name('news.destroy');
});




